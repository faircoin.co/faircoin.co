# FairCoin.Co mainpage

Mainpage and API.

The service is made for/controlled by gitlab runner.

## Usage <small>( by gitlab runner automation )</small>

Go into gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**

Enter variable name **CMD**

#### CMD - commands

~~~
start        # start container ( changes of scripts )
stop         # stop container
uninstall    # remove container
~~~

#### CI/CD Settings

Go Gitlab **Settings** -> **CI/CD** -> **Variables**

~~~
#### FairCoin.Co group variables ######################
ELECTRUMFAIR_CONF        # electrumfair config
erpcuser                 # export from ELECTRUMFAIR_CONF
erpcpassword             # export from ELECTRUMFAIR_CONF
erpcport                 # export from ELECTRUMFAIR_CONF

FAIRCOIND_CONF           # electrumfair config
rpcuser                  # export from FAIRCOIND_CONF
rpcpassword              # export from FAIRCOIND_CONF
rpcport                  # export from FAIRCOIND_CONF

LH_PORT                  # php -S 0.0.0.0:${LH_PORT}
~~~


## Development <small>( manual usage )</small>

If you want create an instance manual then follow the  instructions.

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env
1. run `. ./setenv.sh` to set env vars in your session
1. now you can use `docker-compose` to up/down/start/stop/build service or check the logs
