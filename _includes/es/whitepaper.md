[whitepaper.pdf](/docs/whitepaper_es.pdf){: .btn .btn-sm .btn-cover}
{: .download_pdf }


# FairCoin FreeVision Whitepaper

## Introducción

FairCoin existe desde 2014 bajo la perspectiva de una economía productiva justa y cooperativa. Toda la información al respecto se puede encontrar en https://faircoin.world el sitio web principal de FairCoin.

Uno de los aspectos principales de FairCoin es el mecanismo de estabilización de su precio, para prevenir situaciones perniciosas para una economía justa, como el pump and dump* [Nota al pie: inflar el valor en los mercados y vender masivamente por debajo del precio real (Dumping https://es.wikipedia.org/wiki/Dumping) ].

Otra característica importante es el algoritmo de Prueba de Cooperación (Proof-of-Cooperation) con el que nodos específicos crean los bloques en todas las transacciones utilizando el método round-robin* [Nota al pie: Usado en blockchain privadas donde el minado esta restringido a unas identidades identificables. Por lo  que  Round-Robin,  resuelve  el  dilema  planteado  por  las blockchain  prívadas,  en  el  cual  un participante puede monopolizar el proceso de minado. La solución esta en la restricción del número de nodos que pueden ser creados por el mismo minero dentro de una ventana de tiempo dada. Esto fuerza a Round-Robin, a que los nodos permitidos puedan crear bloques en rotacion para poder generar un bloque válido. ] y casi sin coste alguno. Este tipo de algoritmo  ahorra recursos y escala mejor que un sistema de Prueba de Trabajo (Proof-of-Work) o de Prueba de Participación (Proof-of-Stake), que utilizan otras criptomonedas.

La realidad nos muestra que el mecanismo de precios existente es muy limitado y crea algunas necesidades que son difíciles de cubrir. En principio, el sistema depende de los inversores que inyectan nuevo capital en el sistema que asegura la liquidez necesaria para mantener el precio estable y equilibrado. Durante la creación de este whitepaper el precio actual de mercado de FairCoin es de 0,10€ pero el precio oficial sigue siendo de 1,20€.

FairCoin FreeVision no pretende cuestionar este mecanismo de precios, sino añadir otro instrumento que funcione siempre, en todos los mercados y que no dependa de nuevos inversores o capital.

Queremos AÑADIR otras posibilidades a FairCoin pero no queremos y NUNCA bifurcaremos (crearemos otro Fork) la cadena de bloques de FairCoin. No queremos competir con el mecanismo y la infraestructura existentes, sino que deseamos apoyarlos y quizás podamos mejorar todo el ecosistema de FairCoin con nuestras ideas.

En este momento vemos FairCoin impulsado y promovido por FairCoop como un movimiento eco-humano e idealista. Pero también vemos colectivos eco-humanos similares, como komun.org [ https://komun.org/es ], que ponen mayor énfasis en la descentralización.

Pensamos que si queremos crear una criptomoneda potente que se convierta en una alternativa al dinero FIAT o a Bitcoin, necesitamos difundir FairCoin e invitar a personas que quieren apoyar una economía justa de un modo más realista y pragmático. Nuestro ánimo está en alcanzar acuerdos, en lugar de seguir un solo camino, si ello nos lleva a dar un paso adelante.

FairCoin FreeVision debe entenderse como una parte de la comunidad FairCoin que no quiere ignorar el libre mercado, sino trabajar en él y encontrar un punto medio entre la liquidez y la estabilidad. También nos planteamos una reflexión sobre la descentralización, sobre las posibles mejoras del algoritmo de Prueba de Cooperación (PoC), o la forma de procesar los intercambios de moneda FIAT a FairCoin o de FairCoin a FIAT.

FairCoin FreeVision está hecha para personas que atesoran un gran interés en la libertad y la sostenibilidad, como por ejemplo, los progesistas. Y desea ser el vínculo entre mucha gente que puede ayudarnos a construir una red potente.

Queremos hacer de FairCoin un modelo líquido para las monedas locales y sociales y crear una infraestructura de pago global.


## El mecanismo de precios

Los ajustes de precio no deben ser hechos por los humanos en las asambleas porque pensamos que funciona sólo de forma limitada y no puede representar el movimiento global o la comunidad de miles de miembros de FairCoin. No resulta eficaz que una docena de participantes decida sobre los cambios en su valor, si la comunidad tiene 500 o más miembros. Consideramos que los ajustes de precio de mercado para FairCoin deben hacerse de forma automatizada y derivar el mecanismo de precio fijo a otra moneda como FairCash.


### ¿Por qué preferimos un mecanismo de precios basado en el libre mercado a un mecanismo de precios oficial?

Porque...

* Nuestro precio de apoyo se ajustará en forma de cascada.

* Los precios de venta FreeVision (para el cambio de FairCoin a otras monedas) mediante cascadas logarítmicas estabilizadas en un promedio del 25%.

* Y los precios de compra FreeVision (para el cambio de otras monedas a FairCoin) mediante cascadas logarítmicas estabilizadas en un promedio del 5%.

* Para evitar que se produzcan saltos entre cascadas, se añade a ambos precios una histéresis del 5%.

* Para prevenir cambios de precios provocados por pumps and dumps en el mercado libre, los precios de venta de FreeVision sólo bajarán si los precios de compra en los mercados están cayendo por debajo del precio de venta de FreeVision. El precio de compra de FreeVision sólo subirá si el precio de venta del mercado libre está por encima del precio de compra de FreeVision.

* Para prevenir variaciones en los mercados con baja liquidez, el volumen de oferta y demanda de los precios de mercado se establece en 10.000 FairCoin (cantidad que puede modificarse en el futuro si es necesario). De este modo, las cantidades y transacciones pequeñas no influyen en el mecanismo de precios de FreeVision.


```
define('LOG_NORMALIZED_25',
  Array(
    1,
    1.25,
    1.5,
    1.75,
    2,
    2.5,
    3,
    3.5,
    4,
    5,
    6,
    7,
    8
  )
);

define('LOG_NORMALIZED_5',
  Array(
    1,
    1.05,
    1.1,
    1.15,
    1.2,
    1.25,
    1.3,
    1.35,
    1.4,
    1.5,
    1.6,
    1.7,
    1.8,
    1.9,
    2,
    2.1,
    2.2,
    2.3,
    2.4,
    2.5,
    2.6,
    2.7,
    2.8,
    3,
    3.25,
    3.5,
    3.75,
    4,
    4.25,
    4.5,
    4.75,
    5,
    5.25,
    5.5,
    6,
    6.5,
    7,
    7.5,
    8,
    8.5,
    9,
    9.5
    )
  );
```

El precio de venta de FreeVision es siempre más bajo que el precio del mercado libre. Y el precio de compra es siempre más alto que el precio en los mercados. De esta forma podemos evitar el arbitrage trading* [Nota al pie: operaciones bursáliles determinadas por programas computarizados]y las problemáticas relacionadas con la liquidez.


## El sistema de intercambio

Cambiamos otras monedas a FairCoin a precio de mercado pero el cambio de FairCoin a otras monedas sólo a precio de apoyo. Un ejemplo: Si el precio de mercado es de 0.20€ cambiamos de FairCoin a otras monedas a 0.10€ y el cambio de otras monedas a FairCoin a 0.20€.

Así, no sólo evitamos operaciones de arbitrage trading, sino que el sistema en sí realiza arbitrage trading de tal modo que la ganancia irá hacia el sistema de FairCoin, respaldando el precio oficial de 1.20€, o permitiéndonos invertir los beneficios en mejoras técnicas de provecho para toda la comunidad de FairCoin.

Nuestro sistema de intercambio será abierto y transparente para todas (respetando la privacidad de las usuarias) y procesado por intercambios p2p realizados por colaboradoras autorizadas.


### El sistema de slots

Para que los intercambios sean eficientes, utilizamos un system slot (sistema de ranuras) para organizar todos los intercambios. Cada slot tiene el mismo tamaño, por ejemplo, 1000 FairCoin. Por lo tanto, sólo apoyaremos los intercambios en pasos de 100€.

Por el momento, el número máximo de slots está limitado a 10, teniendo en cuenta los requerimientos del intercambio P2P. Así, las usuarias más pequeñas de FairCoin podrán procesar los intercambios y podemos alcanzar  más fácilmente un buen nivel de descentralización.

Gracias al mecanismo que evita el citado arbitrage trading, los intercambios no suponen riesgos para quienes lo realizan. Para impedir que los arbitrage traders ejecuten intercambios, en una primera etapa FreeVision determinará los miembros confiables y posteriormente se implementará un mecanismo de red de confianza.
