[whitepaper.pdf](/docs/whitepaper_en.pdf){: .btn .btn-sm .btn-cover}
{: .download_pdf }

# FairCoin FreeVision Whitepaper

## Introduction
FairCoin exists since 2014 with the vision to promote a fair and cooperatively functioning economy.
All information can be found at https://faircoin.world on the base FairCoin website.

The focus of FairCoin is the pricing mechanism to stabilize the exchange rate and prevent high-volatility speculation.

Another focus is the proof-of-cooperation algorithm in which specific network nodes in a round robin method generate new blocks (in which the transactions are located). The transaction fee is set to a minimum and plays no role in the transfer of small amounts. This proof-of-cooperation algorithm saves resources and also scales significantly better than other algorithms such as proof-of-work or proof-of-stake.

The current situation shows us that the existing pricing mechanism (the strict determination of 1.20 € / FairCoin) is extremely limiting and can not meet the needs of many participants especially merchants.
The pricing system is also highly dependent on new liquidity especially larger investors which are necessary to keep the price stable and liquid at this strict level.
During the preparation of this paper the market price was only 0.10 €.

FairCoin FreeVision does not want to question the existing price mechanism but it want add a different mechanism that not dependent on investors and works independently of the market situation at all times.

We want to create more prospects for FairCoin but don't want fork of FairCoin Blockchain.
We also do not want to compete but want to improve the entire FairCoin ecosystem with our ideas and solutions.

We currently see FairCoin mainly managed by Fair.Coop a movement / organization that aims to build an alternative economy.
Komun.org another movement / organization pursuing similar goals as Fair.Coop but higher focussed to decentralization.

We think that if we want to create a strong digital currency with FairCoin we have to spread FairCoin as widely as possible. We should also invite those people who are interested in a fair cooperative economy but choose a less idealistic and more pragmatic way.
We are ready to compromises if they can take us one step further.

FairCoin FreeVision should be understood as part of the FairCoin community which does not want to ignore or damn the free market. We want using the free market to create liquidity. We strive for a middle-way between liquidity and stability and decentralization (both technically and organizationally) is very important to us.

FairCoin FreeVision is made for people focussed on freedom and sustainability.

Our vision is making FairCoin a liquid standard for local and social currencies and creating a global standardized payment infrastructure.


## The price mechanism

We think that price adjustments should not be decided by less people in assemblies. We think that a relatively small mass of people in an assembly should not meet the decisions for a mass of FairCoin users.

We think that the price adjustments in FairCoin should be automatic taking into account the free market and the existing price fixes should be outsourced to other digital currencies such as FairCash or other social currencies.

FairCoin as a liquid way of exchange and payment.
FairCash as a very value-stable way of value storage.

### Why we want prefer a free market based price mechanism than an official price mechanism?

Because ...

* our price is adjusted in stages and therefore nevertheless creates a high degree of stability in our free market based mechanism.

* We use an average of 25% steps which were normalized in order to avoid not ergonomic conversion rates. (FreeVision bid) wants to be the reference for example for the exchanges of goods and services.

* For the exchange other currencies in FairCoin we use an average of 5% steps. (FreeVision ask)

* In order to prevent a price change of the FreeVision exchange rate from speculative free market we use a hysteresis of 5% to prevent this effect.

* In addition an increase in the FreeVision price will only be made by increasing the bid price on the open market. Speculative purchases only lead to price increases if the prices on the bid orders side also rise.

* A reduction in the price of FreeVision only occurs through a falling price on the ask orders side.

* In addition we use an order-depth of 10000 FairCoin to prevent price movements by low valued orders. (Adjustments of the order depth in the future is possible)


```
define('LOG_NORMALIZED_25',
  Array(
    1,
    1.25,
    1.5,
    1.75,
    2,
    2.5,
    3,
    3.5,
    4,
    5,
    6,
    7,
    8
  )
);

define('LOG_NORMALIZED_5',
  Array(
    1,
    1.05,
    1.1,
    1.15,
    1.2,
    1.25,
    1.3,
    1.35,
    1.4,
    1.5,
    1.6,
    1.7,
    1.8,
    1.9,
    2,
    2.1,
    2.2,
    2.3,
    2.4,
    2.5,
    2.6,
    2.7,
    2.8,
    3,
    3.25,
    3.5,
    3.75,
    4,
    4.25,
    4.5,
    4.75,
    5,
    5.25,
    5.5,
    6,
    6.5,
    7,
    7.5,
    8,
    8.5,
    9,
    9.5
    )
  );
```

The freevision bid price is everytime lower than the free market price and the freevision ask price is everytime higher than the free market price.

So we can prevent arbitrage trading and the liquidity problematic.


## The exchange system

Basically we exchange other currencies at FairCoin at market prices and FairCoin at other currencies at our FreeVision "bid" price.

The resulting arbitrage gain will be used to preserve and further develop the system.

Our barter system will be open and transparent to everyone (taking into account the privacy of the users) and will be realized through P2P barter platforms.


### The slot system

To make the exchange as comfortable and efficient as possible we use a slot system.

Each slot has a predefined size of currently 1000 FairCoin. An exchange is supported only in a multiple of this magnitude.

The maximum number of slots is limited to currently 10.
10 * 1000 FairCoin = 10000 FairCoin.
We consider this to be necessary in order to fulfill the decentralization and the challenges of a P2P exchange.

Since the FreeVision pricing mechanism does not permit arbitrage speculation externally there are no price risks for the supporting peers at any time.

In order to make it more difficult for speculators to penetrate the exchange system from the outside and realize arbitrage profits we define trustful peers and implemented web-of-trust in the future.
