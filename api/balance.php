<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$RPCusername = $_ENV['erpcuser'];
$RPCpassword = $_ENV['erpcpassword'];
$RPCconnect = '127.0.0.1';
$RPCport = $_ENV['erpcport'];

$faircoinAddress = $_GET['address'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, $RPCusername.':'.$RPCpassword);
curl_setopt($ch, CURLOPT_URL, $RPCconnect.':'.$RPCport);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, '{"id":"curltext","method":"getaddressbalance","params":{"address":"'.$faircoinAddress.'"}}');
curl_setopt($ch, CURLOPT_POST, 1);

$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch))
{
    echo 'Error:'.curl_error($ch).PHP_EOL;
}
else
{
    $result =json_decode($result,true);
	echo json_encode($result['result']);
    //echo($result['result']['confirmed'].PHP_EOL);
    //echo($result['result']['unconfirmed'].PHP_EOL);
}
curl_close ($ch);
exit();

?>
