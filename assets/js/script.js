$( document ).ready(function() {
  $('#language-btn').click(
    function(){
      $('.language-list').toggleClass('d-none');
    }
  );

  $('.language-list > li').click(
    function(){
      window.location.href='?ln=' + $(this).text();
    }
  );
});
